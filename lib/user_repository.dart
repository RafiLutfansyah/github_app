import 'package:mirae_app/github.dart';

import 'api_client.dart';

class UserRepository {
  final _client = ApiClient();

  Future<Github> searchUser(String query) async {
    final response = await _client.get('search/users', <String, dynamic>{
      'q': query,
    });

    return Github.fromJson(response);
  }
}
