import 'package:get/get.dart';
import 'package:mirae_app/github.dart';
import 'package:mirae_app/user_repository.dart';

class HomeController extends GetxController {
  final _userRepo = UserRepository();

  final github = Rx<Github?>(null);
  
  void searchUser(String query) async {
    final response = await _userRepo.searchUser(query);
    github.value = response;
  }
}
