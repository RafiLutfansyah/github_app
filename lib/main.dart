import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'home_controller.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  final _controller = Get.put(HomeController());
  Timer? _debounce;

  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            TextField(
              onChanged: (value) {
                if (_debounce?.isActive ?? false) _debounce?.cancel();
                _debounce = Timer(const Duration(milliseconds: 500), () {
                  _controller.searchUser(value);
                });
              },
            ),
            Obx(
              () => Expanded(
                child: ListView.separated(
                  itemCount: _controller.github.value?.items?.length ?? 0,
                  separatorBuilder: (context, index) {
                    return Divider(height: 0);
                  },
                  itemBuilder: (context, index) {
                    final data = _controller.github.value?.items?[index];

                    return ListTile(
                      title: Text(data?.login ?? '-'),
                      subtitle: Text(data?.url ?? '-'),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
