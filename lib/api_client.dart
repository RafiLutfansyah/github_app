import 'package:dio/dio.dart';

class ApiClient {
  final _dio = Dio(BaseOptions(baseUrl: 'https://api.github.com/'));

  ApiClient();

  Future<dynamic> get(String path, [Map<String, dynamic>? params]) async {
    final response = await _dio.get(path, queryParameters: params);
    return response.data;
  }
}
