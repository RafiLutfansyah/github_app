void main() {
  scan(['c', 'b', 'b', 'a', 'a', 'c', 'd', 'd', 'd']);
}

void scan(List<String> stringArr) {
  Map<String, int> data = Map();

  for (var item in stringArr) {
    data[item] = data[item] == null ? 1 : data[item]! + 1;

    if (data[item] == 3) {
      print(item);
      break;
    }
  }
}
